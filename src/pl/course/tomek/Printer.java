package pl.course.tomek;

public class Printer {

    private int tonerLevel;
    private int paperUsed;
    private boolean duplex;

    public Printer(int tonerLevel, boolean duplex) {
        if (tonerLevel <= 100 && tonerLevel >= 0) {
            this.tonerLevel = tonerLevel;
        } else {
            this.tonerLevel = -1;
        }
        this.paperUsed = 0;
        this.duplex = duplex;
    }

    public int fillUpToner(int amount) {
        if ((this.tonerLevel + amount) > 100 || amount < 0 || this.tonerLevel < 0) {
            System.out.println("Toner error");
            return -1;
        } else {
            this.tonerLevel += amount;
            System.out.println("toner filled up to " + this.tonerLevel + "%");
            return this.tonerLevel;
        }
    }

    public void print(int pages) {
        System.out.println(pages + " pages printed");
        if (duplex) {
            this.paperUsed += (pages / 2) + (pages % 2);
        } else {
            this.paperUsed += pages;
        }
        System.out.println(this.paperUsed + " sheets of paper used in total");
    }
}
